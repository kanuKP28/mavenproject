package com.example.demo;


import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
	@Autowired
	StudentDAO ob;
	@RequestMapping("/hello")
	@GetMapping
	public String display() {
		return "welcome to microservices!" ;
		
	}
	@RequestMapping("/student1")
	public StudentBean display1() {
		return new StudentBean("Kanu","22","Jalandhar");
	}
	@RequestMapping("/student")
	public List<StudentBean> getStudents(){
		return ob.getStudents();
		
	}
	@RequestMapping("/student/{name}")
	public List<StudentBean> getDetails(@PathVariable String name){
		return ob.getDetails(name);
		
	}
	@RequestMapping(method=RequestMethod.POST, value="/student")
	public String insert(@RequestBody StudentBean bean){
		return ob.insert(bean);
		
	}
	@RequestMapping(method=RequestMethod.POST, value="/student1")
	public String delete(@RequestBody String name){
		return ob.delete(name);
		
	}
}
