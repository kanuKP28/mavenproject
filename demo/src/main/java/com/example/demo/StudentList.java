package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service //object will be automatically created
public class StudentList {
	
	private List<StudentBean> list;

	public StudentList() {
		super();
		list=new ArrayList<>();
		list.add(new StudentBean("Kanu","22","Jalandhar"));
		list.add(new StudentBean("Shubham","22","Patiala"));
		list.add(new StudentBean("zayn","25","Delhi"));
		list.add(new StudentBean("Sidhant","20","Jammu"));
		list.add(new StudentBean("Samson","21","Mumbai"));//data shud be taken from JPA and hibernate
	}

	public List<StudentBean> getList() {
		return list;
	}

	public void setList(List<StudentBean> list) {
		this.list = list;
	}
	
	

}
