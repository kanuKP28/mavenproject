package com.example.demo;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {
	Controller o;
	StudentBean ob;
	List<StudentBean> list;
	@Before
	public void init() {
		o= new Controller();
		ob=new StudentBean();
		list=new ArrayList<>();
		list.add(new StudentBean("Kanu","22","Jalandhar"));
		list.add(new StudentBean("Shubham","22","Patiala"));
		list.add(new StudentBean("zayn","25","Delhi"));
		list.add(new StudentBean("Sidhant","20","Jammu"));
		list.add(new StudentBean("Samson","21","Mumbai"));
	}
	
	@Test
	public void test1() {	
		final String url_employees = "http://10.151.60.208:8099/hello";
		RestTemplate resttemp= new RestTemplate();
		String result= resttemp.getForObject(url_employees, String.class);
		assertEquals("welcome to microservices!", result);
	}
	

	@Test
	public void test2() {
		final String URL_EMPLOYEES = "http://10.151.60.208:8099/student";   
		HttpHeaders headers = new HttpHeaders(); 
		headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));    
		headers.setContentType(MediaType.APPLICATION_JSON);    
		HttpEntity<StudentBean[]> entity = new HttpEntity<StudentBean[]>(headers);// RestTemplate    
		RestTemplate restTemplate = new RestTemplate(); // Send request with GET method, and Headers.      
		ResponseEntity<StudentBean[]> response = restTemplate.exchange(URL_EMPLOYEES,HttpMethod.GET, entity, StudentBean[].class);  
		StudentBean[] result = response.getBody();
		//Arrays.asList(result).stream().forEach(System.out::println);	
		List<StudentBean> asresult=Arrays.asList(result);
		asresult.stream().forEach(System.out::println);
		list.stream().forEach(System.out::println);
		assertEquals(list.size(),asresult.size());
		
	}

}
